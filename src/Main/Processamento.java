package Main;

/**
 *
 * @author radames
 */
class Processamento {

    public int somarDoisNumeros(int a, int b) {
        int soma = a + b;
        return soma;
    }

    public double somarDoisNumeros(double a, double b) {
        double soma = a + b;
        return soma;
    }
}
